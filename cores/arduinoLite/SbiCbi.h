/* 
 * File:   SbiCbi.h
 * Author: krupkaf
 *
 * Created on 14. října 2018, 9:25
 */

#ifndef SBICBI_H
#define SBICBI_H

//Bezpecne (z pohledu preruseni) nasteveni a nulovani bitu v IO registru.
//  port |= (1 << bit) je prelozeno na instrukci sbi, pokud je adresa portu mensi nez 0x20. Vetsi adresy instrukce sbi neumoznuje adresovat.
//  U vyssich adres je to prelozeno na posloupnost:
//      tmpReg = port
//      tmpReg |= (1 << bit)
//      port = tmpReg
//  Pokud ale pri vykonavani nastane v prubehu preruseni, ktere si sahne na stejny port stejnym zpusobem,
//  po navratu z preruseni je zmena provedena v preruseni vracena zpet - katastrofa.
//
//  Vsechna makra jsou proti tomuto problemu imunni.
//
//  Makra SBI a CBI se prekladaji na jednu instrukci (rychle), funguji vsak pouze pro adresy portu do 0x1F.
//  Pokud jsou makra pouzita na vyssi adresy, preklad skonci chybou.
//
//  Makra SBIE a CBIE funguji na vsechny adresy. Lze pouzit i na adresy v RAM.
//  Pokud jsou pouzity na adresy mensi nez 0x20, preklad projde normalne. Vysledek je vsak zbytezne neoptimalni.
//  Rucne je potreba rozhodnout zda pouzit SBI/CBI nebo SBIE/CBIE. Najit resini, ktere by to delalo automaticky se mi nepodarilo.
//
//  Makra SBIEM a CBIEM umoznuji zmenit stav vice bitu najednou maskou. Funguji na vsechny adresy. Lze pouzit i na adresy v RAM.
//  Pouziti techto maker muze byt vyhodnejsi nez opakovane pouziti SBI/CBI na jednom portu.

#define SBI(port,bit)  asm("sbi %0, %1" : : "I" (((uint16_t)& port) - 0x20), "I" (bit))
#define CBI(port,bit)  asm("cbi %0, %1" : : "I" (((uint16_t)& port) - 0x20), "I" (bit))

#define SBIE(port,bit)  {register uint8_t tmp = SREG; cli(); port |= (1 << bit); SREG = tmp;}
#define CBIE(port,bit)  {register uint8_t tmp = SREG; cli(); port &= ~(1 << bit); SREG = tmp;}

#define SBIEM(port,bitMask)  {register uint8_t tmp = SREG; cli(); port |= (bitMask); SREG = tmp;}
#define CBIEM(port,bitMask)  {register uint8_t tmp = SREG; cli(); port &= ~(bitMask); SREG = tmp;}

#endif /* SBICBI_H */

