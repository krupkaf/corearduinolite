/*
  pins_arduino.h - Pin definition functions for Arduino
*/

#include "../mega/pins_arduino.h"
#include "pins.h"

#define CONTROLLINO_INIT    MAXI_INIT_RELE      \
                            MAXI_INIT_DO        \
                            MAXI_INIT_OVL       \
                            MAXI_INIT_DI        \
                            MAXI_INIT_RS485

#undef NUM_DIGITAL_PINS 
#define NUM_DIGITAL_PINS            68
#undef NUM_ANALOG_INPUTS
#define NUM_ANALOG_INPUTS           14
#define CONTROLLINO_MAXI
