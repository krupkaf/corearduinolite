#ifndef PINS_INIT_MAXI_H
#define PINS_INIT_MAXI_H

#ifdef __cplusplus
extern "C" {
#endif

#include <SbiCbi.h>

#define MAXI_INIT_RELE  PORTA = 0x00;        \
                        DDRA = 0xFF;         \
                        PORTC &= ~0xC0;      \
                        DDRC |= 0xC0;

#define Rele0_On()    SBI(PORTA,PA0)
#define Rele0_Off()   CBI(PORTA,PA0)
#define Rele1_On()    SBI(PORTA,PA1)
#define Rele1_Off()   CBI(PORTA,PA1)
#define Rele2_On()    SBI(PORTA,PA2)
#define Rele2_Off()   CBI(PORTA,PA2)
#define Rele3_On()    SBI(PORTA,PA3)
#define Rele3_Off()   CBI(PORTA,PA3)
#define Rele4_On()    SBI(PORTA,PA4)
#define Rele4_Off()   CBI(PORTA,PA4)
#define Rele5_On()    SBI(PORTA,PA5)
#define Rele5_Off()   CBI(PORTA,PA5)
#define Rele6_On()    SBI(PORTA,PA6)
#define Rele6_Off()   CBI(PORTA,PA6)
#define Rele7_On()    SBI(PORTA,PA7)
#define Rele7_Off()   CBI(PORTA,PA7)
#define Rele8_On()    SBI(PORTC,PC7)
#define Rele8_Off()   CBI(PORTC,PC7)
#define Rele9_On()    SBI(PORTC,PC6)
#define Rele9_Off()   CBI(PORTC,PC6)

#define MAXI_INIT_DO    PORTE &= ~0x38;      \
                        DDRE |= 0x38;        \
                        PORTG &= ~(1 << PG5);\
                        DDRG |= (1 << PG5);  \
                        PORTH &= ~0x78;      \
                        DDRH |= 0x78;        \
                        PORTB &= ~0xF0;      \
                        DDRB |= 0xF0;

#define D0_On()        SBI(PORTE,PE4)
#define D0_Off()       CBI(PORTE,PE4)
#define D1_On()        SBI(PORTE,PE5)
#define D1_Off()       CBI(PORTE,PE5)
#define D2_On()        SBI(PORTG,PG5)
#define D2_Off()       CBI(PORTG,PG5)
#define D3_On()        SBI(PORTE,PE3)
#define D3_Off()       CBI(PORTE,PE3)
#define D4_On()        SBIE(PORTH,PH3)
#define D4_Off()       CBIE(PORTH,PH3)
#define D5_On()        SBIE(PORTH,PH4)
#define D5_Off()       CBIE(PORTH,PH4)
#define D6_On()        SBIE(PORTH,PH5)
#define D6_Off()       CBIE(PORTH,PH5)
#define D7_On()        SBIE(PORTH,PH6)
#define D7_Off()       CBIE(PORTH,PH6)
#define D8_On()        SBI(PORTB,PB4)
#define D8_Off()       CBI(PORTB,PB4)
#define D9_On()        SBI(PORTB,PB5)
#define D9_Off()       CBI(PORTB,PB5)
#define D10_On()       SBI(PORTB,PB6)
#define D10_Off()      CBI(PORTB,PB6)
#define D11_On()       SBI(PORTB,PB7)
#define D11_Off()      CBI(PORTB,PB7)

#define MAXI_INIT_OVL   PORTE |= (1 << PE7);   \
                        DDRE |= (1 << PE7);
#define OVL_On()       {PORTE &= ~(1 << PE7);}
#define OVL_Off()      {PORTE |= (1 << PE7);}
#define OVL_Tg()       {PORTE ^= (1 << PE7);}

#define MAXI_INIT_DI    DDRF = 0x00;            \
                        PORTF = 0x00;           \
                        DDRK &= ~(0x03);        \
                        PORTK &= ~(0x03);       \
                        DDRD &= ~0x06;          \
                        PORTD &= ~0x06;

#define A0_In()         (PINF & (1 << PF0))
#define A1_In()         (PINF & (1 << PF1))
#define A2_In()         (PINF & (1 << PF2))
#define A3_In()         (PINF & (1 << PF3))
#define A4_In()         (PINF & (1 << PF4))
#define A5_In()         (PINF & (1 << PF5))
#define A6_In()         (PINF & (1 << PF6))
#define A7_In()         (PINF & (1 << PF7))
#define A8_In()         (PINK & (1 << PK0))
#define A9_In()         (PINK & (1 << PK1))
#define IN0_In()        (PIND & (1 << PD3))
#define IN1_In()        (PIND & (1 << PD2))

#define MAXI_INIT_SPI_SS DDRJ |= (1 << PJ2) | (1 << PJ3);   \
                         PORTJ |= (1 << PJ3); \
                         PORTJ &= ~(1 << PJ2);
    //Pozor!!!      W5100 ma SS aktivni v nule a RTC ma SS aktivni v jednicce
#define W5100_SS_Init   SBIE(DDRJ, PJ3)
#define W5100_SS_Set    SBIE(PORTJ, PJ3)
#define W5100_SS_Clr    CBIE(PORTJ, PJ3)
#define RTC_SS_Set      SBIE(PORTJ, PJ2)
#define RTC_SS_Clr      CBIE(PORTJ, PJ2)

#define RS485_RW_Set()  SBIEM(PORTJ,(1 << PJ5) | (1 << PJ6))
#define RS485_RW_Clr()  CBIEM(PORTJ,(1 << PJ5) | (1 << PJ6))
#define MAXI_INIT_RS485  DDRJ |= (1 << PJ1) | (1 << PJ5) | (1 << PJ6);\
                         RS485_RW_Clr();

#ifdef __cplusplus
}
#endif

#endif /* PINS_INIT_MAXI_H */
